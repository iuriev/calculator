var results = document.getElementById("display");
var inputs = ["", "", ""];
var values = [];

var add = function (a, b) {
    return a + b;
};

var subtract = function (a, b) {
    return a - b;
};
var multiply = function (a, b) {
    return a * b;
};
var divide = function (a, b) {
    return a / b;
};

var equals = function () {
    if (inputs[1] === "+") {
        var sum = add(parseFloat(inputs[0]), parseFloat(inputs[2]));
        clear();
        values.push(sum);
    } else if (inputs[1] === "-") {
        var difference = subtract(parseFloat(inputs[0]), parseFloat(inputs[2]));
        clear();
        values.push(difference)
    } else if (inputs[1] === "*") {
        var product = multiply(parseFloat(inputs[0]), parseFloat(inputs[2]));
        clear();
        values.push(product);
    } else if (inputs[1] === "/") {
        var quotient = divide(parseFloat(inputs[0]), parseFloat(inputs[2]));
        clear();
        values.push(quotient);
    }
    display();
};

var update = function (value) {
    inputs.push(value);
    inputs.shift();
};
var clear = function () {
    inputs = ["", "", ""];
    values = [];
    display();
};
var display = function () {
    results.innerHTML = inputs.join(" ") + " " + values.join("");
};

for (var i = 0; i < 10; i++) {
    document.getElementById(i).addEventListener("click", function () {
        values.push(this.innerHTML);
        display();
    });
}
for (var i = 11; i < 15; i++) {
    document.getElementById(i).addEventListener("click", function () {
        update(values.join(""));
        update(this.innerHTML);
        values = [];
        display();
    });
}
document.getElementById(15).addEventListener("click", function () {
    update(values.join(""));
    values = [];
    equals();
});
document.getElementById(16).addEventListener("click", function () {
    clear();
});

